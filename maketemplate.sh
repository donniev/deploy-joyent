#!/bin/bash

#declare -a rArray=$(  yaml2json  docker-compose.yml |jq ".nginx.links" |awk -F: 'BEGIN{out="\"nginx\""}/:/{out=out$1"\" ";ct++}END{print("("out")")}'|sed 's/,)/\)/')

#echo $rArray
declare -a rArray=$(echo '('; cat  docker-compose.yml |grep 'forupstream' |sed 's/\([^+]*\)\+\(.*\)\+\(.*\)$/\2/'|sed 's/\\//g'; echo ')')
echo $rArray
function preRow(){

for i in "${rArray[@]}"
  do
   echo  '(or '
  done
}
function startRow(){

for i in "${rArray[@]}"
  do
   echo  "(eq .Name \"$i\"))"
  done
}


function getPre(){
cat <<EOF
upstream {{.Name}} {\n
        ip_hash; \n
		{{range service .Name}}\n
			server {{.Address}}:{{.Port}};\n
		{{end}}\n
		}\n
EOF
}
function getDefaultHeader(){
cat << EOF
 server {\n
    listen       80;\n
    server_name  _;\n
    location /health.txt {\n
        add_header content-type "text/html";\n
        alias /usr/share/nginx/html/index.html;\n
    }\n
EOF
}
function getDefaultServer(){
cat << EOF
    location /{{.Name}}/ {\n
        proxy_pass http://{{.Name}}/;\n
        proxy_redirect off;\n
        proxy_http_version 1.1;\n
		proxy_set_header Upgrade \$http_upgrade;\n
		proxy_set_header Connection "upgrade";\n
		proxy_set_header Host \$host;\n
		proxy_set_header X-Forwarded-Host \$host;\n
		proxy_set_header X-Forwarded-Server \$host;\n
		proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;\n
		proxy_set_header X-Real-IP \$remote_addr;\n
    }\n
      location = /{{.Name}} {\n
            return 302 /{{.Name}}/;\n
        }\n

EOF
}
function getDefaultFooter(){
cat <<EOF
    {{end}}\n
}\n
EOF
}
function getSingleService(){
cat <<EOF
{{if .Tags}}\n
server {\n
 server_name {{range .Tags }} {{ . |replaceAll "-" "."}} {{end}} ;\n
	listen 80 ;\n
	location / {\n
	   proxy_redirect off;\n
			  proxy_http_version 1.1;\n
			proxy_set_header Upgrade \$http_upgrade;\n
			proxy_set_header Connection "upgrade";\n
			proxy_set_header Host \$host;\n
			proxy_set_header X-Forwarded-Host \$host;\n
			proxy_set_header X-Forwarded-Server \$host;\n
			proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;\n
			proxy_set_header X-Real-IP \$remote_addr;\n
			proxy_pass http://{{.Name}}/;\n
	}\n
	location = "" {\n
		   return 302 /;\n
	 }\n
}\n
{{end}}\n
EOF

}
echo '{{range services }}'>nginx/default.ctmpl
needspost=$(echo '{{if '$(preRow) $(startRow) '}}')
echo $needspost >>nginx/default.ctmpl
echo -e $(getPre)>>nginx/default.ctmpl
echo '{{end}}'>>nginx/default.ctmpl
echo '{{end}}'>>nginx/default.ctmpl
echo -e $(getDefaultHeader)>>nginx/default.ctmpl
echo '{{range services }}'>>nginx/default.ctmpl
echo $needspost >>nginx/default.ctmpl
echo -e $(getDefaultServer)>>nginx/default.ctmpl
echo '{{end}}'>>nginx/default.ctmpl
echo -e $(getDefaultFooter)>>nginx/default.ctmpl
echo '{{range services }}'>>nginx/default.ctmpl
echo $needspost >>nginx/default.ctmpl
echo -e $(getSingleService)>>nginx/default.ctmpl
echo '{{end}}'>>nginx/default.ctmpl
echo '{{end}}'>>nginx/default.ctmpl


