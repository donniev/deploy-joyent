#!/bin/bash
PROGNAME=$(basename $0)

function bail
{

#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------


	echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
	exit 1
}

COMPOSE_CFG=
APPPREFIX=example
#source ~/.bash_profile
while getopts "f:p:r:c:" optchar; do
    case "${optchar}" in
        f) COMPOSE_CFG=" -f ${OPTARG}" ;;
        p) APPPREFIX=${OPTARG} ;;
        r) REGION=${OPTARG} ;;
        c) CONSUL_NUMBER=${OPTARG} ;;
    esac
done
shift $(expr $OPTIND - 1 )
source setuptriton.sh
#echo -e $(env |grep -E '(MANT|DNS|SDC)')
export COMPOSE_HTTP_TIMEOUT=600
COMPOSE_CFG=docker-compose-local.yml
./maketemplate.sh || bail
COMPOSE="docker-compose -p ${APPPREFIX} -f ${COMPOSE_CFG:-}"
CONFIG_FILE=${COMPOSE_CFG:-docker-compose.yml}
CONSUL_NUMBER=${CONSUL_NUMBER:-1}
echo "project prefix:      $APPPREFIX"
echo "docker-compose file: $CONFIG_FILE"
echo "region: $REGION"

echo 'Pulling latest container versions'
${COMPOSE} pull consul || bail

echo 'Starting Consul.'
export BOOTSTRAP_HOST=
echo "On start $BOOTSTRAP_HOST"
${COMPOSE} up -d --no-recreate consul || bail

# Wait for the bootstrap instance
echo
echo -n 'Waiting for the bootstrap instance.'

export BOOTSTRAP_HOST="127.0.0.1"
#export BOOTSTRAP_HOST="$(triton inst get ${APPPREFIX}_consul_1 | json -a ips.0)"

BOOTSTRAP_UI="${BOOTSTRAP_HOST}"
echo "After first up $BOOTSTRAP_HOST"
ISRESPONSIVE=0
while [ $ISRESPONSIVE != 1 ]; do
    echo -n '.'

    curl -fs --connect-timeout 1 http://$BOOTSTRAP_UI:8500/ui &> /dev/null
    if [ $? -ne 0 ]
    then
        sleep 1
    else
        let ISRESPONSIVE=1
    fi
done
echo
echo 'The bootstrap instance is now running'
echo "Dashboard: $BOOTSTRAP_UI:8500/ui/"
#command -v open >/dev/null 2>&1 && `open http://$BOOTSTRAP_UI:8500/ui/`


echo 'Scaling the Consul raft to three nodes'
echo "docker-compose -p ${APPPREFIX} scale consul=3"
${COMPOSE}  scale consul=${CONSUL_NUMBER} || bail
echo "After Scaling $BOOTSTRAP_HOST";



# get network info from consul and poll it for liveness
echo "${APPPREFIX}_consul_1"
    CONSUL_IP=${CONSUL_IP:-"127.0.0.1"}

sleep 60
echo "Writing template values to Consul at ${CONSUL_IP}"
while :
do
    # we'll sometimes get an HTTP500 here if consul hasn't completed
    # it's leader election on boot yet, so poll till we get a good response.
    sleep 1
    curl --fail --max-time 30 -X PUT --data-binary @./nginx/default.ctmpl \
         http://${CONSUL_IP}:8500/v1/kv/nginx/template && break
    echo -ne .
done

echo
echo 'Copying consul ip'
#open http://${CONSUL_IP}:8500/ui
echo "${CONSUL_IP}" > /tmp/consulip

#echo 'Starting Nginx'
#${COMPOSE} up -d --no-deps nginx

