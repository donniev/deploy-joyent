#!/bin/bash
PROGNAME=$(basename $0)

function bail
{

#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------


	echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
	exit 1
}

COMPOSE_CFG=
APPPREFIX=example
while getopts "f:p:r:" optchar; do
    case "${optchar}" in
        f) COMPOSE_CFG=" -f ${OPTARG}" ;;
        p) APPPREFIX=${OPTARG} ;;
        r) REGION=${OPTARG} ;;
    esac
done
shift $(expr $OPTIND - 1 )
#echo -e $(env |grep -E '(MANT|DNS|SDC)')

COMPOSE_CFG=docker-compose-local.yml

COMPOSE="docker-compose -p ${APPPREFIX} -f ${COMPOSE_CFG:-}"
CONFIG_FILE=${COMPOSE_CFG:-docker-compose.yml}
${COMPOSE} pull nginx
echo 'starting the nginx container'
${COMPOSE}  up -d --no-recreate --no-deps nginx || bail "Could not start nginx"

