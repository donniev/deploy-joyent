# deploy-joyent

## 2016-01-14 DLV 0.0.1
1. Initial commit

## 2016-01-14 DLV 0.0.1
1. Typos

## 2016-01-27 DLV 0.0.2
1. Added "exec" to Service options. Prompts for command and then does docker exec with that command on container

## 2016-01-27 DLV 0.0.3
1. Added "Uptime " to Service options. Shows status, start time, and Number of restarts using docker inpect
## 2016-02-11 DLV 0.0.4
1. Better handling of bad choice in menu selection
## 2016-02-12 DLV 0.0.5
1. Changes to template so doesn't emit record if port is 0
## 2016-02-13 DLV 0.0.6
1. Found some hard coded paths
## 2016-02-15 DLV 0.0.7
1. Changed method of creating nginx template so only services that are linked to nginx affect template
1. Requires yaml2json to be installed globally
## 2016-02-15 DLV 0.0.8
1. Extraneous files in publish. No code changes
## 2016-02-15 DLV 0.0.9
1. Added utility to manually update DNS for services not under nginx conrol
## 2016-02-16 DLV 0.0.10
1. Missed a section in maketemplate

## 2016-02-16 DLV 0.0.11
1. refactored determine of upstream
1. Added menu item to reload base template if you add a service to docker-compose after nginx has been deployed







