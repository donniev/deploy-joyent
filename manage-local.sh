#!/usr/bin/env bash

PROGNAME=$(basename $0)

function bail
{

#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------


	echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
	exit 1
}

PREFIX=$1
PREFIX=${PREFIX:-test}
echo "Using prefix: $PREFIX"
echo $(env |grep SDC_URL)
MAIN="DSR Start Status Open-Consul Instances Services Reloadbase QUIT"
QUIT=" QUIT"
SERVICES=$(cat docker-compose-local.yml |awk '$0 ~ /^[^ ]+:$/{print $NF}' |sed 's/://g')
echo Services available: $SERVICES
CHOICES="Start Uptime Inspect Exec Scale Shell DockerLogs Stop Remove UpdateDNS StopRemoveRestart Done"
INSTCHOICES="Uptime Inspect Exec Shell DockerLogs Stop Remove UpdateDNS Done"
SCALES="1 2 3 4 5"
options=("Consul_1 Nginx_1" "Redis" "SocketServer" "BloomFilter" "Hexo" "Crypto" "Ninja" "Cron" "F24"  )

function menu() {
    echo "Avaliable options:"
    for i in ${!options[@]}; do
        printf "%3d%s) %s\n" $((i+1)) "${choices[i]:- }" "${options[i]}"
    done
    [[ "$msg" ]] && echo "$msg"; :
}
function startup {
prompt='Check an option (again to uncheck, ENTER when done): '
while menu && read -rp "$prompt" num && [[ "$num" ]]; do
    [[ "$num" != *[![:digit:]]* ]] &&
    (( num > 0 && num <= ${#options[@]} )) ||
    { msg="Invalid option: $num"; continue; }
    ((num--)); msg=""
    [[ "${choices[num]}" ]] && choices[num]="" || choices[num]="+"
done



    [[ "${choices[0]}" ]] && (export CONSUL_NUMBER=1 && ./startConsul-local.sh -c 1 -p ${PREFIX} -r local  && ./startNginx-local.sh   -p ${PREFIX} -r local  || bail ) ;
    [[ "${choices[1]}" ]] && (./startservice-local.sh -p ${PREFIX} -s redis -r local || bail) ;
    [[ "${choices[2]}" ]] && (./startservice-local.sh -p ${PREFIX} -s socketserver -r local || bail) ;
    [[ "${choices[3]}" ]] && (./startservice-local.sh -p ${PREFIX} -s bloomfilter -r local || bail) ;
    [[ "${choices[4]}" ]] && (./startservice-local.sh -p ${PREFIX} -s hexo -r local || bail) ;
    [[ "${choices[5]}" ]] && (./startservice-local.sh -p ${PREFIX} -s crypto -r local || bail) ;
    [[ "${choices[6]}" ]] && (./startservice-local.sh -p ${PREFIX} -s ninja -r local || bail) ;
    [[ "${choices[7]}" ]] && (./startservice-local.sh -p ${PREFIX} -s cron -r local || bail) ;
    [[ "${choices[8]}" ]] && (./startservice-local.sh -p ${PREFIX} -s f24 -r local || bail) ;

    MAIN="DSR Status Open-Consul Instances Services Reloadbase QUIT"
   doit

}



function Reloadbase(){
./reloadbase.sh -p ${PREFIX}  -r ${REGION}
			clear
			echo "base template reloaded"
			doit
}

function showdockerlogs(){
	select opt2 in ${SERVICES}; do
	if [ "$opt2" = "QUIT" ]; then
		echo Done
		clear
		doit
	else
declare -a RESULTS=$(docker ps -a |grep $opt2 |awk '{out[$NF]++}END{for (i in out) print i}')
		select optstop in ${RESULTS}; do
		/usr/local/bin/docker logs ${optstop} | grep -v -E  -'^127\.0\.0\.1'
		doit
		done
	fi
	done
}
function stopit(){
declare -a RESULTS=$(docker ps -a |grep $optservice |awk '{out[$NF]++}END{for (i in out) print i}')
declare -i linecount=$(docker ps |grep $optservice|wc -l)
	if [ "$linecount" -eq  1 ]; then
	    /usr/local/bin/docker stop ${RESULTS}
	   doit
	else
		select optstop in ${RESULTS}; do
		/usr/local/bin/docker stop ${optstop}
		doit
		done
	fi

}
function removeit(){
declare -a RESULTS=$(docker ps -a |grep $optservice |awk '{out[$NF]++}END{for (i in out) print i}')
declare -i linecount=$(docker ps |grep $optservice|wc -l)
	if [ "$linecount" -eq  1 ]; then
	    /usr/local/bin/docker stop ${RESULTS}
	    /usr/local/bin/docker rm ${RESULTS}
	    doit
	else
			select optstop in ${RESULTS}; do
		/usr/local/bin/docker stop ${optstop}
		/usr/local/bin/docker rm ${optstop}
		doit
		done
   fi
}
function inspect(){
	declare -a RESULTS=$(docker ps -a |grep $optservice |awk '{out[$NF]++}END{for (i in out) print i}')
	declare -i linecount=$(docker ps |grep $optservice|wc -l)
	if [ "$linecount" -eq  1 ]; then
	    /usr/local/bin/docker inspect ${RESULTS}
	    echo
		if [ "$NEXT" = "services" ]; then
		   showservice
		else
		   showinstance
		fi
	else
		select optstop in ${RESULTS}; do
		/usr/local/bin/docker inspect ${optstop}
		echo
		if [ "$NEXT" = "services" ]; then
		   showservice
		else
		   showinstance
		fi
		done
	fi



}
function inspectinstance(){
	/usr/local/bin/docker inspect $optservice

}
function uptime(){
	declare -a RESULTS=$(docker ps -a |grep $optservice |awk '{out[$NF]++}END{for (i in out) print i}')
	declare -i linecount=$(docker ps |grep $optservice|wc -l)
	if [ "$linecount" -eq  1 ]; then
	    /usr/local/bin/docker inspect ${RESULTS}  |jq ".[0]| {Service:\"${optservice}\",Running: .State.Running,since:.State.StartedAt,Restarts:.RestartCount }"
	    echo
		if [ "$NEXT" = "services" ]; then
		   showservice
		else
		   showinstance
		fi
	else
		select optstop in ${RESULTS}; do
		echo Service:${optstop}
		/usr/local/bin/docker inspect ${optstop}  |jq ".[0]| {Service:\"${optservice}\",Running: .State.Running,since:.State.StartedAt,Restarts:.RestartCount }"
		echo
		if [ "$NEXT" = "services" ]; then
		   showservices
		else
		   showinstances
		fi
		done
   fi



}
function execCommand(){
	declare -a RESULTS=$(docker ps -a |grep $optservice |awk '{out[$NF]++}END{for (i in out) print i}')
	declare -i linecount=$(docker ps |grep $optservice|wc -l)
	if [ "$linecount" -eq  1 ]; then
	   echo "Enter the command:"
	    read cmd
	    /usr/local/bin/docker exec ${RESULTS} ${cmd}
	    echo
		if [ "$NEXT" = "services" ]; then
		   showservice
		else
		   showinstance
		fi
	else
		select optstop in ${RESULTS}; do
		echo "Enter the command:"
	    read cmd
		/usr/local/bin/docker exec ${optstop} ${cmd}
		echo
		if [ "$NEXT" = "services" ]; then
		   showservice
		else
		   showinstance
		fi
		done
    fi


}
function startit() {
	echo "Starting ${PREFIX} in ${REGION}"
	startup
	#./start.sh -p ${PREFIX} -r ${REGION}
	#MAIN="DSR Status tail-nginx Open-Consul Services reloadbase QUIT"
	#clear
	#echo "Consul and nginx started"
	#doit

}
function openconsul(){
	CONSUL_IP=docker.internal
	CONSUL_IP=${CONSUL_IP:-localhost}
	open http://${CONSUL_IP}:8500/ui
	clear
	echo "Consul console opened"
	doit
}
function deletestopremove(){
#	/usr/local/bin/docker exec ${PREFIX}_nginx_1 /usr/local/bin/node /application/index.js --task deletedns
	/usr/local/bin/docker stop $(/usr/local/bin/docker ps -q)
	/usr/local/bin/docker rm $(/usr/local/bin/docker ps -aq)
	clear
	echo "Containers stopped and removed"
	doit
}
function initialize(){
	declare -i RESULTS=$(/usr/local/bin/docker ps |grep 'nginx' |wc -l)
	if [ "${RESULTS}" -lt 1 ]; then
		echo Current region: $REGION nginx and consul not running
		MAIN="DSR Start Status Open-Consul Instances Services Change_Region Reloadbase QUIT"
	else
		echo Current region: $REGION nginx and consul up and running
		MAIN="DSR Status Open-Consul Instances Services Change_Region Reloadbase QUIT"
	fi

}
function updateDNS(){
  read -p "Domain Name: " CF_ROOT_DOMAIN
   read -p "Record Name: " RecordName
    RecordName=$(echo "${RecordName}.${CF_ROOT_DOMAIN}")
   echo "updating $RecordName at $CF_ROOT_DOMAIN  with current IP for $optservice"
   export CF_ROOT_DOMAIN=$CF_ROOT_DOMAIN
   ./update-dns.sh $optservice $RecordName ${PREFIX} ${CF_TTL}
   echo

   doit
}
function showservice(){
	select opt3 in ${CHOICES}; do
	if [ "$opt3" = "Start" ]; then
	declare -i	RESULTS=$(/usr/local/bin/docker ps |  grep ${optservice}|wc -l)
		if [ "${RESULTS}" -lt 1 ]; then
			./startservice-local.sh -p ${PREFIX} -s ${optservice} -r ${REGION}
			clear
			echo "$optservice started"
			showservices
		else
			echo "Service already started"
			showservices
		fi
	elif [ "$opt3" = "Stop" ]; then
		stopit
		showservices
	elif [ "$opt3" = "Uptime" ]; then
		uptime
		showservice
	elif [ "$opt3" = "Inspect" ]; then
		inspect
		showservice
	elif [ "$opt3" = "Exec" ]; then
		execCommand
	elif [ "$opt3" = "UpdateDNS" ]; then
		updateDNS
	elif [ "$opt3" = "Remove" ]; then
		removeit
		showservices
	elif [ "$opt3" = "StopRemoveRestart" ]; then
		 declare -a RESULTS=$(docker ps -a |grep $optservice |awk '{out[$NF]++}END{for (i in out) print i}')
		 /usr/local/bin/docker stop ${RESULTS} && \
	     /usr/local/bin/docker rm ${RESULTS}  && \
		./startservice-local.sh -p ${PREFIX} -s ${optservice} -r ${REGION} && \
		showservices
	elif [ "$opt3" = "DockerLogs" ]; then
		declare -a RESULTS=$(docker ps -a |grep $optservice |awk '{out[$NF]++}END{for (i in out) print i}')
		declare -i linecount=$(docker ps |grep $optservice|wc -l)
		if [ "$linecount" -eq  1 ]; then
	    /usr/local/bin/docker logs ${RESULTS}
	    echo
		showservice
		else
		select optstop in ${RESULTS}; do
				/usr/local/bin/docker logs ${optstop}
				echo ""
				echo "Docker logs for ${optdoc} finished"
				showservice
				done
		 fi

	elif [ "$opt3" = "Shell" ]; then
		echo $optservice
		declare -a RESULTS=$(docker ps  |grep $optservice |awk '{out[$NF]++}END{for (i in out) print i}')
         declare -i linecount=$(docker ps |grep $optservice|wc -l)
	if [ "$linecount" -eq  1 ]; then
	    /usr/local/bin/docker exec -it ${RESULTS} bash
	    echo
		if [ "$NEXT" = "services" ]; then
		   showservice
		else
		   showinstance
		fi
	else
			select optshell in ${RESULTS}; do
			/usr/local/bin/docker exec -it ${optshell} bash
			if [ "$NEXT" = "services" ]; then
		   showservice
		else
		   showinstance
		fi
			done
    fi

	elif [ "$opt3" = "Done" ]; then
		if [ "$NEXT" = "services" ]; then
		   showservices
		else
		   showinstances
		fi
	elif [ "$opt3" = "Scale" ]; then
		select opt4 in ${SCALES}; do
		echo "${opt4}"
		/usr/local/bin/docker-compose -p ${PREFIX} scale ${optservice}=${opt4}
		showservice
		done
	else
		echo "Wat u talkin bout, Willis"
		showservice
	fi
	done




}
function showinstance(){
	select opt3 in ${INSTCHOICES}; do
	if [ "$opt3" = "Stop" ]; then
	    echo "Stopping $optservice"
		triton inst stop $optservice
		showinstances
	elif [ "$opt3" = "Uptime" ]; then
		uptime
		showinstance
	elif [ "$opt3" = "Inspect" ]; then
		inspectinstance
		showinstance
	elif [ "$opt3" = "Exec" ]; then
		execCommand
	elif [ "$opt3" = "UpdateDNS" ]; then
		updateDNS
	elif [ "$opt3" = "Remove" ]; then
		removeit
		showinstances
	elif [ "$opt3" = "DockerLogs" ]; then
	    /usr/local/bin/docker logs -f ${optservice}
	    echo
		showinstance
	elif [ "$opt3" = "Shell" ]; then
	    /usr/local/bin/docker exec -it ${optservice} bash
	    echo
		showinstance
	elif [ "$opt3" = "Done" ]; then
		if [ "$NEXT" = "services" ]; then
		   showservices
		else
		   showinstances
		fi
	else
		echo "Wat u talkin bout, Willis"
		showinstance
	fi
	done




}


function showservices() {
SERVICES2=$(echo $SERVICES  && echo 'Done')
	NEXT='services'
	select optservice in ${SERVICES2}; do
	if [ "$optservice" = "Done" ]; then
		doit
	fi
	if [ -z "$optservice" ]; then
 	 echo "Wat u talkin bout, Willis"
		doit
	else
		declare -i RESULTS=$(/usr/local/bin/docker ps | grep ${optservice}|wc -l)
		if [ "${RESULTS}" -lt 1 ]; then
			CHOICES="Start Uptime Inspect Exec Scale Shell DockerLogs Remove StopRemoveRestart Done"
		else
			CHOICES="Stop Uptime Inspect Exec Scale Shell DockerLogs Remove UpdateDNS StopRemoveRestart Done"
		fi
      	showservice
    fi
	done
}
function showinstances() {
SERVICES2=$(triton inst ls -o name |grep -v NAME |sort  && echo 'Done')
	NEXT='instances'
	select optservice in ${SERVICES2}; do
	if [ "$optservice" = "Done" ]; then
		doit
	fi
	if [ -z "$optservice" ]; then
 	 echo "Wat u talkin bout, Willis"
		doit
	else
		declare -i RESULTS=$(/usr/local/bin/docker ps | grep ${optservice}|wc -l)
		if [ "${RESULTS}" -lt 1 ]; then
			CHOICES="Start Uptime Inspect Exec Shell DockerLogs Remove Done"
		else
			CHOICES="Stop Uptime Inspect Exec Scale Shell DockerLogs Remove UpdateDNS Done"
		fi
      	showinstance
    fi
	done
}
function doit() {
	initialize
	select opt in ${MAIN}; do
	if [ "$opt" = "QUIT" ]; then
		echo Done
		exit
	elif [ "$opt" = "Reloadbase" ]; then
		Reloadbase
	elif [ "$opt" = "Start" ]; then
		startit
	elif [ "$opt" = "Open-Consul" ]; then
		openconsul
	elif [ "$opt" = "Status" ]; then
		echo "services running"
		#triton inst ls  -o name,state,primaryIp,age |sort
		/usr/local/bin/docker ps | awk '{print $NF}' | sort
		doit
	elif [ "$opt" = "DSR" ]; then
		deletestopremove
	elif [ "$opt" = "DockerLogs" ]; then
		showdockerlogs
	elif [ "$opt" = "tail-nginx" ]; then
		/usr/local/bin/docker exec ${PREFIX}_nginx_1 tail -f /var/log/nginx/access.log
		echo ""
		echo "Nginx tail finished"
		doit
	elif [ "$opt" = "Services" ]; then
	    showservices
	elif [ "$opt" = "Instances" ]; then
	    showinstances
	else
		echo "Wat u talkin bout, Willis"
		doit
	fi
	done
}
function chooseregion {


	REGION=local
	source ./setuptriton.sh
	doit
}
chooseregion

