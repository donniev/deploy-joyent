#!/bin/bash
PROGNAME=$(basename $0)

function bail
{

#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------


	echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
	exit 1
}

COMPOSE_CFG=
APPPREFIX=example
while getopts "f:p:r:" optchar; do
    case "${optchar}" in
        f) COMPOSE_CFG=" -f ${OPTARG}" ;;
        p) APPPREFIX=${OPTARG} ;;
        r) REGION=${OPTARG} ;;
    esac
done
shift $(expr $OPTIND - 1 )
TRITON=${REGION:-sw}
source setuptriton.sh triton $TRITON
#echo -e $(env |grep -E '(MANT|DNS|SDC)')

COMPOSE_CFG=

COMPOSE="docker-compose -p ${APPPREFIX}${COMPOSE_CFG:-}"
CONFIG_FILE=${COMPOSE_CFG:-docker-compose.yml}

echo 'Scaling the nginx'
docker-compose -p ${APPPREFIX} scale nginx=2 || bail

