#!/bin/bash
# Note that this is a bash script but works properly if sourced into .zshrc as well

# Docker configuration
export DOCKER_TLS_VERIFY=1
export COMPOSE_HTTP_TIMEOUT=300
# Triton configuration
function jpc-config {
    export JOYENT_KEYFILE=${HOME}/.ssh/id_rsa

    # Manta configuration
    export MANTA_USER=XXXX
    export MANTA_URL=https://us-east.manta.joyent.com
    export MANTA_KEY_ID=$(ssh-keygen -l -f $JOYENT_KEYFILE | cut -d' ' -f2)

    # SDC configuration for Joyent Public Cloud
    export SDC_ACCOUNT=XXXX
    export SDC_KEY_ID=${MANTA_KEY_ID}
    export SDC_URL=https://us-sw-1.api.joyentcloud.com
    export DOCKER_CERT_PATH=${HOME}/.sdc/docker/${SDC_ACCOUNT}
    export DOCKER_HOST=tcp://us-sw-1.docker.joyent.com:2376
}

# switch environment to connect to Docker locally
function docker-local {
    case $1 in
        *)
            if [ $(docker-machine status default) != "Running" ]
            then
                docker-machine start default
            fi
            eval "$(docker-machine env default)"
            # I write this to /etc/hosts so that I can hit containers from my browser
            # at http://docker.internal:<port>/ without looking up and transcribing
            # the IP every time. (This is portable between BSD and GNU sed.)
            sudo sed -i.bak "/docker\.internal/ s/.*/$(docker-machine ip default) docker.internal/" /etc/hosts 
            ;;
    esac
}
function docker-aws {
         jpc-config # reset our environment
          case $1 in
            XXXX)
            eval "$(docker-machine env XXXX )"
            ;;
            *)
             eval "$(docker-machine env XXXX )"
             ;;
             esac
}

# switch environment to connect to Triton
function docker-triton {
    jpc-config # reset our environment
    case $1 in
        
         east)
            export DOCKER_HOST=tcp://us-east-1.docker.joyent.com:2376
            export SDC_URL=https://us-east-1.api.joyentcloud.com
            ;;
         eu|ams|amsterdam)
            export DOCKER_HOST=tcp://eu-ams-1.docker.joyent.com:2376
            export SDC_URL=https://eu-ams-1.api.joyentcloud.com/
            ;;
        sw|vegas)
            export DOCKER_HOST=tcp://us-sw-1.docker.joyent.com:2376
            export SDC_URL=https://us-sw-1.api.joyentcloud.com
;;
        *)
           export DOCKER_HOST=tcp://us-east-1.docker.joyent.com:2376
            export SDC_URL=https://us-east-1.api.joyentcloud.com
            ;;

    esac
}

# Set us up to work with Docker locally by default, but with our Manta and SDC
# environment ready to be used.
jpc-config
case $1 in 
  local)
docker-local $2
   ;;
triton)
   docker-triton $2
  ;;
aws)
   docker-aws $2
   ;;
*)
docker-local $1
 ;;
  esac

