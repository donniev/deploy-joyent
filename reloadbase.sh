#!/bin/bash

COMPOSE_CFG=
APPPREFIX=example
source ~/.bash_profile
while getopts "f:p:s:r:" optchar; do
    case "${optchar}" in
        f) COMPOSE_CFG=" -f ${OPTARG}" ;;
        p) APPPREFIX=${OPTARG} ;;
        s) SERVICE=${OPTARG} ;;
        r) REGION=${OPTARG} ;;
    esac
done
TRITON=${REGION:-sw}
source setuptriton.sh triton $TRITON
echo $(env |grep SDC)
COMPOSE="docker-compose -p ${APPPREFIX}${COMPOSE_CFG:-}"
CONFIG_FILE=${COMPOSE_CFG:-docker-compose.yml}
./maketemplate.sh
echo "${APPPREFIX}_consul_1"
if [ -z "${COMPOSE_CFG}" ]; then
    CONSUL_IP=$(sdc-listmachines --name ${APPPREFIX}_consul_1 | json -a ips.1)
else
    CONSUL_IP=${CONSUL_IP:-$(docker-machine ip default)}
fi


echo "Writing template values to Consul at ${CONSUL_IP}"
while :
do
    # we'll sometimes get an HTTP500 here if consul hasn't completed
    # it's leader election on boot yet, so poll till we get a good response.
    sleep 1
    curl --fail  -X PUT --data-binary @./nginx/default.ctmpl \
         http://${CONSUL_IP}:8500/v1/kv/nginx/template && break
    echo -ne .
done