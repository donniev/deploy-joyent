#!/bin/bash
PROGNAME=$(basename $0)

function bail
{

#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------


	echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
	exit 1
}

COMPOSE_CFG=docker-compose-local.yml
APPPREFIX=example
#source ~/.bash_profile || bail9
while getopts "f:p:s:r:" optchar; do
    case "${optchar}" in
        f) COMPOSE_CFG=" -f ${OPTARG}" ;;
        p) APPPREFIX=${OPTARG} ;;
        s) SERVICE=${OPTARG} ;;
        r) REGION=${OPTARG} ;;
    esac
done
COMPOSE="docker-compose -p ${APPPREFIX} -f ${COMPOSE_CFG:-}"
CONFIG_FILE=${COMPOSE_CFG:-docker-compose.yml}
echo $COMPOSE
echo 'Pulling latest container versions'
${COMPOSE} pull $SERVICE || bail

CONSUL_IP=${CONSUL_IP:-$(docker-machine ip default)}
echo "$SERVICE"
${COMPOSE} up -d --no-recreate --no-deps $SERVICE || bail

