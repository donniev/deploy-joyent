# deploy-joyent

This is a set of scripts to control services that use Joyent containerbuddy along with nginx and consul

1. [ContainerBuddy](https://github.com/joyent/containerbuddy)
1. [Consul](https://www.consul.io/docs/agent/http.html)

There are five scripts

1. setuptriton_template.sh which configures your environment
	You will need to modify that file to put your user information and then copy it to setuptriton.sh
1. start.sh a modified version of the start script from container buddy which launches consul and nginx
1. start.sh which starts and individual service
1. manage.sh which wraps all this in command line menus
1. maketemplate.sh which creates the nginx template file. Note that this requires yaml2json to be installed globally

You will also need docker-compose.yml. See the docker-compose and container buddy documentation for more information.
default.ctmpl is used by container buddy to configure nginx. This version is slightly different than the default version.
See [blog](http://blog.vawter.com/2016/01/02/Updating-Nginx-on-Joyent-Triton-Using-ContainerBuddy-Consul-Nginx-and-Manta/) for details.
dnsConfig.json is used in that process also.

Environment variables: The following are expected to be present
1. ${HOME}
1. ${SDC_ACCOUNT}

Your key for Joyent is expected to be found in ${HOME}/.ssh/id_rsa
You can, of course, modify setuptriton.sh to change that.

You launch the process with manage.sh
```
./manage.sh <prefix>
```
All your containers will be prepended with prefix. If you don't provide a prefix "test" will be used.

For example:
prod_nginx_1  would be the first instance of the nginx container if you used the prefix "prod"

## The menus

The main menu:

```
1) DSR            3) tail-nginx     5) Services       7) reloadbase
2) Status         4) open-consul    6) Change_Region  8) QUIT

```

1. DSR - Stop and Remove all containers in the region. Nice for development but be careful!
2. Status - Shows a list of running containers
3. tail-nginx - tails the access.log for nginx. Ctrl-c to quit
4. open-consul - Opens the consul gui in a browser (works on Mac. No idea if it works elsewhere)
5. Services - Opens a sub-menu for controlling individual services
6. Change_Region - Switch regions
7. Reload base template
8. Quit - exit the application

The services submenu:

```
1) consul        3) hexo          5) socketserver  7) ninja
2) nginx         4) notify        6) crypto        8) QUIT
```

The list of services defined in your docker-compose.yml
Selecting a service will open the service submenu. If you have more than one instance you will be prompted to select the instance.

The service submenu

```
1) Stop          3) Inspect      5) Scale        7) DockerLogs   9) UpdateDNS
2) Uptime        4) Exec         6) Shell        8) Remove      10) Done


```

If the service is not running Stop will be replaced with Start and UpdateDNS will not be shown

1. Start/Stop - Starts or stops the container. When starting it first pulls the lastest image.
1. Uptime - Shows the status, start time and number of restarts (uses docker inspect)
2. Inspect - runs docker inspect on the container
3. Exec - prompts for a command and then runs docker exec on the container with that command
3. Scales - creates more instances of the container. Currently hard code to 1,2,3,4,5. 1 makes no sense but it is really annoying if the menu prompt number differs from the instances
4. Shell - runs  docker exec -it xxx bash on the container
5. DockerLogs - runs docker logs on the container
6. Remove - Stops and removes the container.
6. UpdateDNS -If you want to manually update the DNS record using the external ip of the service. DO NOT USE THIS FOR SERVICES BEHIND NGINX which automatically updates DNS.
7. Done - returns to the main menu.

UpdateDNS
This should only be used when you have a service not running behind nginx. The nginx container automatically updates the DNS records for containers under it's control.
If, however, you have additional services you can use this utility. The utility will prompt you for the domain and record:

```
Domain Name: dailycryptogram.com
Record Name: redis2
```

It will then use docker inspect to grab the external ip and update (or add) the record at CloudFlare. The update-dns.sh is a modification of that provided in the ContainerBuddy CloudFlare examples. The only modification is that we are grabbing the external not internal ip and are not using Consul.

## Setting the upstreams for nginx

You set the upstreams by adding a comment line in docker-compose.yml. This allows you complete control rather than using "links" in the nginx service definition.

```
## 'forupstream+"consul" "crypto" hexo  "nginx" "ninja" "socketserver" "notify"+'"
```
## Setting server names in nginx
The server names for a service is created from the tags array in the service definition. If the array is empty no server block is created

```
{
  "consul": "consul:8500",
  "services": [
    {
      "name": "ninja",
      "port": 3500,
      "tags": [
        "joyent.lendingclubninja.com",
        "joyent2.lendingclubninja.com",
        "www.lendingclubninja.com",
        "lendingclubninja.com"
      ],
      "health": "/usr/bin/curl --fail -s  -o /dev/null http://localhost:3500/",
      "poll": 10,
      "ttl": 25
    }
  ],
  "backends": [
    {
      "name": "nginx",
      "poll": 7,
      "onChange": "/opt/containerbuddy/reload-ninja.sh"
    }
  ]
}
```
So for the ninja service nginx will route external requests for the domains "joyent.lendingclubninja.com", "joyent2.lendingclubninja.com", "www.lendingclubninja.com", "lendingclubninja.com" to the ninja upstream.

