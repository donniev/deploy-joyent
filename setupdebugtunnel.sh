#!/usr/bin/env bash
ssh -N -q -o StrictHostKeyChecking=no root@docker.internal  \
-L 127.0.0.1:5858:192.168.99.100:5858 \
-L 127.0.0.1:8095:192.168.99.100:8081
